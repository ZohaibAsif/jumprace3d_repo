﻿using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine;

public class SimpleTrampoline : TrampolineManager
{
	[FoldoutGroup("Jump Heights")]
	public float _heightForJump;

	[FoldoutGroup("Jump Speed")]
	public float _speedForJump;

	[FoldoutGroup("Wave Data")]
	public SpriteRenderer _wave;

	[FoldoutGroup("Wave Data")]
	public Color32 _waveColor;

	public override void MakeObjectJump(GameObject _object, AnimationCurve _anim)
	{
		DOTween.Kill("boost " + _object.name);
		_object.transform.DOMoveY(_heightForJump, _speedForJump).SetRelative().SetSpeedBased().SetEase(_anim).SetId("boost " + _object.name)
				.OnComplete(() => { _object.GetComponent<PlayerTrampolineInteractions>().CharacterStateDecider(); });

		MakeEffectOnTrampoline();
	}

	public void MakeEffectOnTrampoline()
	{
		gameObject.transform.DOMoveY(-0.4f, 0.2f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutSine).SetDelay(0.1f);
		_wave.DOColor(_waveColor, 0.35f).SetLoops(2, LoopType.Yoyo);
		_wave.transform.DOScale(0.01f, 0.35f).SetLoops(2, LoopType.Yoyo);
	}
}
