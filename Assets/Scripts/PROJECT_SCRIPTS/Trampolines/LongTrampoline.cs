﻿using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine;

public class LongTrampoline : TrampolineManager
{
    [FoldoutGroup("Jump Heights")]
    public float _heightForJump;


    [FoldoutGroup("Jump Speed")]
    public float _speedForJump;

    public override void MakeObjectJump(GameObject _object, AnimationCurve _anim)
    {
        DOTween.Kill("boost " + _object.name);
        _object.transform.DOMoveY(_heightForJump, _speedForJump).SetRelative().SetSpeedBased().SetEase(_anim).SetId("boost " + _object.name)
                .OnComplete(() => { _object.GetComponent<PlayerTrampolineInteractions>().CharacterStateDecider(); });
    }
}
