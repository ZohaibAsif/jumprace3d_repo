﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
	[FoldoutGroup("Trampolines Data")]
	public List<GameObject> _trampolines;

	[FoldoutGroup("Enemies Data")]
	public List<EnemyAi> _enemies;

	private void Start()
	{
		InitData();
	}

	public void InitData()
	{
		for (int i = 0; i < _enemies.Count; i++)
		{
			_enemies[i]._trampolines = _trampolines;
		}
	}
}
