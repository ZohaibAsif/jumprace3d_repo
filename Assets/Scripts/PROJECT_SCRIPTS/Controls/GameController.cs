﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

public class GameController : Singleton<GameController>
{
	#region Variables

	[FoldoutGroup("Character Parent To Move Forward")]
	public GameObject _cpToMoveForward;

	[FoldoutGroup("Character To Jump & Animate")]
	public GameObject _characterObj;

	[FoldoutGroup("Character To Jump & Animate")]
	public Animator _characterAnim;

	[FoldoutGroup("Speeds of Characters")]
	public float _forwardSpeed;

	[FoldoutGroup("Speeds of Characters")]
	public float _rotationSpeed;

	[FoldoutGroup("Particles Data")]
	public ParticleSystem _winParticles;

	internal bool _canMoveForward = false;
	internal bool _canRotateChar = false;
	private bool _playedOnce = false;

	#endregion

	#region Unity Functions

	private void Update()
	{
		MakeCharacterMoveForward();
		SwipeToRotateCharacter();
		MakeGamePlayAble();
	}

	#endregion

	#region Character & Camera Input

	public void SwipeToRotateCharacter()
	{
		if (Input.GetMouseButton(0) && _canMoveForward && _canRotateChar)
		{
			float _input = CrossPlatformInputManager.GetAxis("Horizontal");
			_cpToMoveForward.transform.eulerAngles = new Vector3(_cpToMoveForward.transform.eulerAngles.x,
																	_cpToMoveForward.transform.eulerAngles.y + _input * _rotationSpeed * Time.deltaTime,
																			_cpToMoveForward.transform.eulerAngles.z);
		}
	}

	public void MakeCharacterMoveForward()
	{
		if (Input.GetMouseButton(0) && _canMoveForward)
		{
			_cpToMoveForward.transform.Translate(Vector3.forward * _forwardSpeed * Time.deltaTime, Space.Self);
		}
	}

	#endregion

	#region Character Start Jumping First

	public void CharacterTriggerable()
	{
		if (!_playedOnce)
		{
			_cpToMoveForward.transform.DOMoveY(-0.08f, 0.15f).SetRelative().OnStart(() => { _characterAnim.SetTrigger("Bounce"); })
					.OnComplete(() => { _cpToMoveForward.GetComponent<Collider>().enabled = true; _characterAnim.ResetTrigger("Bounce"); });
			_playedOnce = true;
			Utilities._canPlayGame = false;
		}
	}

	#endregion

	#region Utilities CallBacks

	public void MakeGamePlayAble()
	{
		if (Utilities._canPlayGame)
		{
			CharacterTriggerable();
		}
	}

	#endregion

	#region Level Win & Lose Work

	public void MakeLevelWin()
	{
		_winParticles.Play();
		UiManager.Instance.LevelComplete();
		_canMoveForward = false;
	}

	public void MakeLevelLose()
	{
		//_winParticles.Play();
		_cpToMoveForward.SetActive(false);
		DOTween.Kill(_cpToMoveForward.transform);
		UiManager.Instance.LevelFail();
		_canMoveForward = false;
	}

	#endregion

}
