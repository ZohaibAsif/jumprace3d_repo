﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class PlayerTrampolineInteractions : MonoBehaviour
{
	public enum CharacterType { Player, Enemy };

	[FoldoutGroup("Character Type")]
	[EnumPaging] public CharacterType _charType = CharacterType.Player;

	[FoldoutGroup("Variables to Control Jump & Heights")]
	public AnimationCurve _easeAnim;

	[FoldoutGroup("Character To Jump & Animate")]
	public GameObject _characterObj;
	[FoldoutGroup("Character To Jump & Animate")]
	public Animator _characterAnim;

	[FoldoutGroup("Free Fall Data")]
	public AnimationCurve _easeAnimFall;
	[FoldoutGroup("Free Fall Data")]
	public float _maxFallHeight;
	[FoldoutGroup("Free Fall Data")]
	public float _speedForFall;

	internal TrampolineManager _TM;
	internal bool _isTriggeredCheck = false;

	#region TriggerWork

	private void OnTriggerEnter(Collider other)
	{
		_TM = other.gameObject.GetComponent<TrampolineManager>();
		if (_TM != null)
		{
			_isTriggeredCheck = true;
			_TM.MakeObjectJump(gameObject, _easeAnim);
			MakeCharacterPerformAcrobat();
			_TM = null;
		}
	}

	#endregion

	#region MakeCharacterWin&Relax

	public void MakeCharacterRelax(GameObject _ref)
	{
		DOTween.Kill("fall" + gameObject.name);
		DOTween.Kill("boost " + gameObject.name);
		gameObject.transform.DOMove(_ref.transform.position, 0.5f).SetDelay(0.1f).OnStart(() => { _characterAnim.SetTrigger("Victory"); });
	}

	#endregion

	#region Character Acrobats & Free Fall

	public void MakeCharacterPerformAcrobat()
	{
		//gameObject.transform.DORotate(Vector3.zero, 0.1f).OnStart(() => { GameController.Instance._canRotateChar = false; })
		//	.OnComplete(() => { GameController.Instance._canRotateChar = true; });

		_characterObj.transform.DOLocalMove(Vector3.zero, 0.2f);
		int _random = Random.RandomRange(0, 3);
		_characterAnim.SetTrigger("Flip" + _random);
		DOTween.Kill("fall" + gameObject.name);
		_characterAnim.ResetTrigger("FreeFall");

		if (IsInvoking(nameof(DelayInStateSwitching)))
		{
			CancelInvoke(nameof(DelayInStateSwitching));
		}

		if (!IsInvoking(nameof(ResetTrigger)))
		{
			Invoke(nameof(ResetTrigger), 0.2f);
		}
	}

	public void ResetTrigger()
	{
		_isTriggeredCheck = false;
	}

	public void CharacterStateDecider()
	{
		switch (_charType)
		{
			case CharacterType.Player:
				if (!_isTriggeredCheck)
				{
					MakeObjectFreeFall();
				}
				break;

			case CharacterType.Enemy:

				gameObject.GetComponent<EnemyAi>().CustomInput();
				break;
		}
	}

	public void MakeObjectFreeFall()
	{
		gameObject.transform.DOMoveY(-_maxFallHeight, _speedForFall).SetRelative().SetSpeedBased()
				.SetEase(_easeAnimFall).SetId("fall" + gameObject.name).OnStart(() => { _characterAnim.SetTrigger("Fall"); FallStateSwitcher(); });
	}

	public void FallStateSwitcher()
	{
		if (!IsInvoking(nameof(DelayInStateSwitching)))
		{
			Invoke(nameof(DelayInStateSwitching), 0.5f);
		}
	}

	public void DelayInStateSwitching()
	{
		_characterAnim.SetTrigger("FreeFall");
	}

	#endregion
}
