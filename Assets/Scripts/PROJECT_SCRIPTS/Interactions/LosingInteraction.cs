﻿using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine;

public class LosingInteraction : MonoBehaviour
{
	internal PlayerTrampolineInteractions _PTI;

	private void OnTriggerEnter(Collider other)
	{
		_PTI = other.gameObject.GetComponent<PlayerTrampolineInteractions>();
		if (_PTI != null)
		{
			GameController.Instance.MakeLevelLose();
			print("Losing");
			_PTI = null;
		}
	}
}
