﻿using Sirenix.OdinInspector;
using DG.Tweening;
using UnityEngine;


public class WinningTrampoline : MonoBehaviour
{
	[FoldoutGroup("Refrence To Land")]
	public GameObject _refToLand;

	internal PlayerTrampolineInteractions _PTI;

	private void OnTriggerEnter(Collider other)
	{
		_PTI = other.gameObject.GetComponent<PlayerTrampolineInteractions>();
		if (_PTI != null)
		{
			_PTI.MakeCharacterRelax(_refToLand);
			GameController.Instance.MakeLevelWin();
			print("Winning");
			_PTI = null;
		}
	}
}
