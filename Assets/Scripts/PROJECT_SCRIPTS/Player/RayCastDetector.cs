﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class RayCastDetector : MonoBehaviour
{
	[FoldoutGroup("Cylinder Object")]
	public GameObject _cylinder;

	[FoldoutGroup("Object To Hit Last")]
	[ReadOnly] public GameObject _lastHit;

	[FoldoutGroup("Vector Position Of Hitting Object")]
	[ReadOnly] public Vector3 _collisionPosition = Vector3.zero;

	[FoldoutGroup("Vector Position Of Hitting Object")]
	[ReadOnly] public Vector3 _pointToCheckExactLand = Vector3.zero;

	[FoldoutGroup("Layer To Hit")]
	public LayerMask _layerToHit;

	[FoldoutGroup("Value To Offset")]
	public float value = 43.5f;

	private void Update()
	{
		var ray = new Ray(this.transform.position, -Vector3.up);
		RaycastHit _hit;
		if (Physics.Raycast(ray, out _hit, 1000, _layerToHit))
		{
			_lastHit = _hit.transform.gameObject;
			_collisionPosition = _hit.point;
			_pointToCheckExactLand = _hit.transform.InverseTransformPoint(_collisionPosition);
			Debug.DrawLine(transform.position, _hit.point, Color.green);
		}

		if (_lastHit.layer == LayerMask.NameToLayer("Tramp"))
		{
			_cylinder.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color32(153, 233, 163, 100);
		}

		else if (_lastHit.layer == LayerMask.NameToLayer("Water"))
		{
			_cylinder.transform.GetChild(0).GetComponent<Renderer>().material.color = new Color32(233, 162, 153, 100);
		}


		float dist = Vector3.Distance(_cylinder.transform.position, _lastHit.transform.position);

		Vector3 newScale = Vector3.Lerp(new Vector3(0.25f, 0f, 0.25f), new Vector3(0.25f, 20f, 0.25f), dist / value);
		_cylinder.transform.localScale = new Vector3(0.25f, newScale.y, 0.25f);
	}
}
