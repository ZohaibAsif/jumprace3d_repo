﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class EnemyAi : MonoBehaviour
{
	public enum EnemyLevel { Noob, Average, Pro };

	[FoldoutGroup("Enemy Attribute States")]
	[EnumPaging] public EnemyLevel _enemyLevel = EnemyLevel.Noob;

	[FoldoutGroup("Character Parent To Move Forward")]
	public GameObject _cpToMoveForward;

	[FoldoutGroup("Character To Jump & Animate")]
	public GameObject _characterObj;

	[FoldoutGroup("Character To Jump & Animate")]
	public Animator _characterAnim;

	[FoldoutGroup("Speeds of Characters")]
	public float _forwardSpeed;

	[FoldoutGroup("Speeds of Characters")]
	public float _rotationSpeed;

	[FoldoutGroup("AI Data")]
	[ReadOnly] public List<GameObject> _trampolines;

	[FoldoutGroup("AI Data")]
	public AnimationCurve _aICurve;

	[FoldoutGroup("AI Data")]
	public int _countToWait = 0;

	[FoldoutGroup("AI Data")]
	public int _currentTrampCount = 0;

	private bool _playedOnce = false;


	private void Update()
	{
		MakeGamePlayAble();
	}


	public void CharacterTriggerable()
	{
		if (!_playedOnce)
		{
			_cpToMoveForward.transform.DOMoveY(-0.08f, 0.15f).SetRelative().OnStart(() => { _characterAnim.SetTrigger("Bounce"); })
					.OnComplete(() => { _cpToMoveForward.GetComponent<Collider>().enabled = true; _characterAnim.ResetTrigger("Bounce"); });
			_playedOnce = true;
			CustomInput();
		}
	}

	public void CustomInput()
	{
		if (_countToWait > 0)
		{
			_countToWait--;
		}
		else if (_countToWait == 0)
		{
			AIWork();
		}
	}

	public void AIWork()
	{
		print("AI");

		switch (_enemyLevel)
		{
			case EnemyLevel.Noob:

				_countToWait = Random.RandomRange(1, 7);
				break;

			case EnemyLevel.Average:

				_countToWait = Random.RandomRange(1, 5);
				break;

			case EnemyLevel.Pro:

				_countToWait = Random.RandomRange(1, 3);
				break;
		}

		gameObject.transform.DOMoveY(_trampolines[_currentTrampCount + 1].transform.position.y, 1f).SetSpeedBased().SetEase(_aICurve);
		gameObject.transform.DOMoveZ(_trampolines[_currentTrampCount + 1].transform.position.z + 0.25f, 5f).SetSpeedBased();
		gameObject.transform.DOMoveX(_trampolines[_currentTrampCount + 1].transform.position.x, 3f).SetSpeedBased();
		_currentTrampCount++;
	}

	#region Utilities CallBacks

	public void MakeGamePlayAble()
	{
		if (Utilities._canPlayGame)
		{
			CharacterTriggerable();
		}
	}

	#endregion
}
