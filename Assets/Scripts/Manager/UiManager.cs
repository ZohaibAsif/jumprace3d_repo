﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using Sirenix.OdinInspector;

public class UiManager : Singleton<UiManager>
{
	#region Variables

	[TabGroup("UI Panels")]
	[SerializeField] private GameObject _tapToPlayPanel;
	[TabGroup("UI Panels")]
	[SerializeField] private GameObject _gamePlayPanel;
	[TabGroup("UI Panels")]
	[SerializeField] private GameObject _levelCompletePanel;
	[TabGroup("UI Panels")]
	[SerializeField] private GameObject _levelFailPanel;

	[TabGroup("Other Data")]
	[SerializeField] private GameObject _controlPanel;

	[TabGroup("Other Data")]
	[SerializeField] private TextMeshProUGUI _levelText;

	#endregion

	#region UnityFunctions

	private void Start()
	{
		_levelText.text = "Level No " + (Utilities.CurrentLevel + 1);
	}

	#endregion

	#region Panels Work

	public void HideTapToPlayPanel()
	{
		_tapToPlayPanel.SetActive(false);
		_gamePlayPanel.SetActive(true);
		DOTween.Kill("TapToStartImage");
		GameController.Instance._canRotateChar = true;
		Utilities._canPlayGame = true;
	}

	public void LevelFail()
	{
		_gamePlayPanel.SetActive(false);
		Invoke(nameof(LevelFailedDelay), 1f);
	}

	public void LevelComplete()
	{
		_gamePlayPanel.SetActive(false);
		Invoke(nameof(LevelCompleteDelay), 2f);
	}

	private void LevelFailedDelay()
	{
		_levelFailPanel.SetActive(true);
	}

	private void LevelCompleteDelay()
	{
		_levelCompletePanel.SetActive(true);
	}

	public void NextLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
		Utilities.CurrentLevel++;
	}

	public void RestartLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}

	#endregion

}
