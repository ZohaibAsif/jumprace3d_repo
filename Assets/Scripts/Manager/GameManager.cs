﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager>
{
	[FoldoutGroup("Level Data")]
	public List<GameObject> _levels;

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.LoadLevel(Application.loadedLevel);
		}

		if (Utilities.CurrentLevel == 3)
		{
			Utilities.CurrentLevel = 0;
		}
	}

	private void Awake()
	{
		_levels[Utilities.CurrentLevel].SetActive(true);
	}
}
