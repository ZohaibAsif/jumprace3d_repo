﻿using UnityEngine;

public static class Utilities
{
    #region StaticVariables

    public static bool _canPlayGame = false;

    #endregion

    #region PrefSystem

    public static int CurrentLevel
    {
        get { return PlayerPrefs.GetInt("Mylevel", 0); }
        set { PlayerPrefs.SetInt("Mylevel", value); }
    }

    public static int GameIndex
    {
        get { return PlayerPrefs.GetInt("MyIndex", 0); }
        set { PlayerPrefs.SetInt("MyIndex", value); }
    }

    public static int Coins
    {
        get { return PlayerPrefs.GetInt("MyCoins", 0); }
        set { PlayerPrefs.SetInt("MyCoins", value); }
    }

    public static int Score
    {
        get { return PlayerPrefs.GetInt("MyScore", 0); }
        set { PlayerPrefs.SetInt("MyScore", value); }
    }

    #endregion
}
